// получаем данные элементов страницы

const inputTask = document.querySelector('.task-input'),            
      addTaskBtn = document.getElementById('addTaskButton'),
      errorSpan = document.getElementById('error'),
      tasksWrapper = document.getElementById('tasksWrapper'),
      redButton = document.querySelector('.red'),
      yellowButton = document.querySelector('.yellow'),
      greenButton = document.querySelector('.green');

let tasks;
let color;
let todoItems;

// проверка на содержимое localstorage

if(!localStorage.tasks) {     // если localstorage пустой - обнуляем массив      
    tasks = [];
} else {
    tasks = JSON.parse(localStorage.getItem('tasks'));   // если нет, то распаршиваем его содержимое в localstorage   
}

// фукнция возврата шаблона таски

const getHtmlTemplate = (task, index) => {          
    return `
        <li class="task ${task.color}">
            <input type="checkbox" id="completeCheckbox" class="done-task-btn" onclick="completeTask(${index})" ${task.complete ? "checked" : ''}>
            <span id="taskTitle" class="task-title ${task.complete ? "completed" : ''}">${task.description}</span>
            <button class="delete-task-btn" onclick="deleteTask(${task.id})">X</button>
        </li>
    `;
};

// фукнция заполнения листа тасок

const fillHtml = () => {                
    tasksWrapper.innerHTML = '';
    if(tasks.length > 0) {
        tasks.forEach((item, index) => {
            tasksWrapper.innerHTML += getHtmlTemplate(item, index);
        });
        todoItems = document.querySelectorAll('.task');         // получем коллекцию всех тасок
    }
};

// функция выполнения (перечеркивания) таски

const completeTask = index => {         
    tasks[index].complete = !tasks[index].complete;
    if(tasks[index].complete) {
        todoItems[index].classList.add('completed');
    } else {
        todoItems[index].classList.remove('completed');
    }
    fillHtml();         // заполняем лист тасок 
    updateLocal();          // обновляем localstorage 
}

// фукнция удаления таски

const deleteTask = (id) => {
    tasks = tasks.filter(task => task.id !== id);           // перезаписываем отфильтрованный массив тасок по айди
    fillHtml();         // заполняем лист тасок
    updateLocal();          // обновляем localstorage
}

// функция конструктор объекта (таски) с её свойствами

function Task(description, color, id) {
    this.id = id;
    this.description = description;         
    this.complete = false;          
    this.color = color;         
}

// функция обновления localstorage

const updateLocal = () => {  
    localStorage.setItem('tasks', JSON.stringify(tasks));
}

// обработчик события на клик по кнопке Add

addTaskBtn.addEventListener('click', () => {            
    if(inputTask.value.length >= 5){            // проверка инпута на количество символов (не меньше пяти)                       
        tasks.push(new Task(inputTask.value, color, Date.now()));           // пушим в массив объект таски с её свойствами
        fillHtml();
        updateLocal();
        inputTask.value = '';      // очищаем инпут после добавления таски
        errorSpan.innerHTML = '';           // удаляем ошибку если до этого она была
        errorSpan.classList.remove('incorrect-value');
    } else if (inputTask.value === '') {         // проверка инпута на пустое поле
        alert('Текст карточки не может быть пустым!');
        inputTask.value = '';
    } else {           
        errorSpan.innerHTML = 'Текст карточки не может быть менее 5 символов';          // вывод ошибки под инпутом
        errorSpan.classList.add('incorrect-value');
    }
    color = '';         // обнуляем цвет следующей карточки
    redButton.removeAttribute('style');         // убираем цвета кнопок измененные ранее
    yellowButton.removeAttribute('style');
    greenButton.removeAttribute('style');
    addTaskBtn.removeAttribute('style');
});

// обработчик события по клику на красную кнопку

redButton.addEventListener('click', () => {
    color = 'red-bg';       // присваиваем фону карточки соответствующий кнопке фон
    redButton.setAttribute('style', 'background-color: #ff3535');           // делаем кнопку визуально включенной
    yellowButton.removeAttribute('style');
    greenButton.removeAttribute('style');
    addTaskBtn.setAttribute('style', 'background-color: #c10000');      // красим кнопку add в соответствующий цвет
});

// обработчик события по клику на желтую кнопку

yellowButton.addEventListener('click', () => {
    color = 'yellow-bg';
    yellowButton.setAttribute('style', 'background-color: #ffdd00');
    redButton.removeAttribute('style');
    greenButton.removeAttribute('style');
    addTaskBtn.setAttribute('style', 'background-color: #dec000');
});

// обработчик события по клику на зеленую кнопку

greenButton.addEventListener('click', () => {
    color = 'green-bg';
    greenButton.setAttribute('style', 'background-color: #00ff0d');
    redButton.removeAttribute('style');
    yellowButton.removeAttribute('style');
    addTaskBtn.setAttribute('style', 'background-color: #00c80a');
});


fillHtml();